# Appunti version control



## Random info

Git contiene tutti i dati necessari al suo funzionamento nella cartella .git

Il **.gitignore** specifica quali file e cartelle devono essere ignorati dal tracciamento.\
simboli:\
*.log -> tutti i file che finiscono in .log\
temp? -> tutti i file che corrispondono a temp+qualcosa "temp1", "temp2"\
crtll/**/*.log -> tutti i .log nella cartella e sottocartelle di crtll\
!important.log -> nega la condizione ignore *.log  solo per questo file

**HEAD** indica il commit in cui mi trovo\
Ogni volta che faccio un nuovo commit in un branch, il ramo corrente avanza e la HEAD si sposta automaticamente sul nuovo commit.

Quando mi separo da un branch con comandi come "git checkout 'commit'" la "HEAD" entra in modalità "detached HEAD"\
quindi ogni nuovo commit non farà parte di un ramo e verrà smarrito negli inferi del .git\
si può risolvere creando un nuovo branch "git branch nome_branch" e "git checkout nome_branch".

## Generale
```
git init
#inizializza una working directory :O

git status
#Mostra lo stato dei file nel repository.

git add
#Aggiunge i file al repository per il commit (area di staging).

git commit
#Crea un nuovo commit con le modifiche dei file aggiunti.

git log
#Mostra la cronologia dei commit.

git clone
#Crea una copia locale di un repository remoto.

git push
#Carica i commit locali nel repository remoto.

git pull
#Aggiorna il repository locale con le modifiche dal repository remoto.

git branch
#Mostra l'elenco dei branch presenti nel repository locale.

git diff
#Mostra le differenze tra il lavoro corrente e l'ultima versione commitata.

git rm <file>
#Rimuove un file in modo tracciato

git mv <file>
#Sposta e rinomina un file
```

## Navigazione branch
```
git branch <nome-branch>
#Crea un nuovo branch con il nome specificato.

git checkout <nome-branch>
#Passa al branch specificato.
```

# Annullamento modifiche
```
git checkout <nome_file>
#Annulla le modifiche ripristinando il file alla versione precedente 

git reset <commit>
#ritorna al commit spiecificato e mantiene i file pronti per essere aggiunti all'area di staging

git reset --hard <commit>
#ritorna al commit specificato e elimina tutti i commit che erano stati fatti dopo di esso.
```

# Integrazione modifiche
```
git merge <nome_branch>
#Unisce i commit del branch specificato nel branch corrente in un unico nuovo commit.

git rebase
#Riproduce i commit del branch da unire e li applica uno dopo l'altro sopra il commit di destinazione
```

# Cassetto del caos
```
git stash
#archiviare le modifiche locali non committate

git stash apply
#ripristina le modifiche stashate

git stash drop
#elimina lo stash
```

# Cherry-pick
```
git cherry-pick <commit id>
#applica le modifiche di un commit specifico proveniente da un branch esterno
```

# Etichette
```
git tag
#visualizza tutti i tag

git tag <nome_del_tag>
#crea un tag. esempio <v1.0>
```
